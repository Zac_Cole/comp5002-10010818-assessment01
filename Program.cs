﻿//start
using System;

namespace Assessment
{
    class Program
    {
        
        static void Main(string[] args)
        {

//declare all the variables 
           var y = "yes";
           var accept = "";
           var name = "";
           double number = 0;
           double number2 = 0;
           double gst = 1.15;

//Start the program with Clear()
            Console.Clear();

//write welcome to the store
            Console.WriteLine("Welcome to my store\n");
// display welcome to the store

//write to user about entering name           
            Console.WriteLine("What is your name? ");
//display
            name = (Console.ReadLine());
//read user input
//write hello
            Console.WriteLine($"Hello {name}");
//display hello (name)
//write about entering number with 2 decimal places
            Console.WriteLine("Please enter a number with 2 decimal places");
// display enter a number with 2 decimal places            
            number = double.Parse(Console.ReadLine());
// read input            

//write would you like to enter another number 
           Console.WriteLine($"would you like to enter another number, yes or no?");
//display would you like to enter another number           
            accept = (Console.ReadLine());
// read input 

           if (accept == y)
           
            {
//write for 2 decimal places if yes(true)                
            Console.WriteLine("Please enter another number with 2 decimal places");
//display enter another number with 2 decimal places            
            number2 = double.Parse(Console.ReadLine());
//read input
//calculate numbers entered   
            Console.WriteLine($"Your total numbers together {(number + number2)}\n");
//display numbers entered             
            }
           else     
            {
//calculate numbers entered                 
            Console.WriteLine($"Total numbers {number}\n");
// display numbers entered            
            }
//write and add GST to the numbers entered
            Console.WriteLine($"total nmbers including GST {(number + number2) * gst}");
//show numbers including gst          
//write for thankyou for shopping with us   
            Console.WriteLine("Thankyou for shopping with us, please come again");
//display thankyou for shopping with us 

          //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
             
//finish
        }
    }
}
